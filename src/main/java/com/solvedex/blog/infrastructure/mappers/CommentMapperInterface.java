package com.solvedex.blog.infrastructure.mappers;

import com.solvedex.blog.application.dto.comment.CommentDto;
import com.solvedex.blog.domain.entities.Comment;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = { BlogUserMapperInterface.class })
public interface CommentMapperInterface {
    CommentMapperInterface MAPPER = Mappers.getMapper( CommentMapperInterface.class );

    @Mapping(source = "commentId", target = "id")
    @Mapping(source = "username", target = "blogUser.username")
    @Mapping(source = "content", target = "description")
    @Mapping(source = "postId", target = "post.id")
    Comment toComment(CommentDto commentDto);

    @InheritInverseConfiguration
    CommentDto fromComment(Comment comment);
}

package com.solvedex.blog.infrastructure.mappers;

import com.solvedex.blog.application.dto.post.PostDto;
import com.solvedex.blog.domain.entities.Post;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = { CommentMapperInterface.class, BlogUserMapperInterface.class })
public interface PostMapperInterface {

    PostMapperInterface MAPPER = Mappers.getMapper( PostMapperInterface.class );

    @Mapping(source = "postId", target = "id")
    @Mapping(source = "username", target = "blogUser.username")
    @Mapping(source = "title", target = "title")
    @Mapping(source = "content", target = "description")
    @Mapping(source = "creationDate", target = "createdAt")
    @Mapping(source = "comments", target = "comments")
    Post toPost(PostDto postDto);

    @InheritInverseConfiguration
    PostDto fromPost(Post post);
}

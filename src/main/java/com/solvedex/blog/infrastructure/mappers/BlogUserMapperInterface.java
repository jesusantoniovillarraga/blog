package com.solvedex.blog.infrastructure.mappers;

import com.solvedex.blog.application.dto.bloguser.BlogUserDto;
import com.solvedex.blog.domain.entities.BlogUser;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = { PostMapperInterface.class })
public interface BlogUserMapperInterface {

    BlogUserMapperInterface MAPPER = Mappers.getMapper( BlogUserMapperInterface.class );

    @Mapping(source = "username", target = "username")
    @Mapping(source = "email", target = "email")
    @Mapping(source = "posts", target = "posts")
    BlogUser toBlogUser(BlogUserDto blogUserDto);

    @InheritInverseConfiguration
    BlogUserDto fromBlogUser(BlogUser blogUser);
}

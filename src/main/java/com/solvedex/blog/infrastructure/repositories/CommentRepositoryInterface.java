package com.solvedex.blog.infrastructure.repositories;

import com.solvedex.blog.domain.entities.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRepositoryInterface extends JpaRepository<Comment,Integer> {
}

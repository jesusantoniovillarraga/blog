package com.solvedex.blog.infrastructure.repositories;

import com.solvedex.blog.domain.entities.BlogUser;
import com.solvedex.blog.domain.entities.Post;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PostRepositoryInterface extends JpaRepository<Post,Integer> {

    @EntityGraph(attributePaths = {"comments"})
    Optional<Post> findById(Integer id);
}

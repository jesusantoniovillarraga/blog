package com.solvedex.blog.infrastructure.repositories;

import com.solvedex.blog.domain.entities.BlogUser;
import com.solvedex.blog.domain.entities.Comment;
import com.solvedex.blog.domain.entities.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BlogUserRepositoryInterface extends JpaRepository<BlogUser, Integer> {
    List<Post> findPostsByUsername(String username);

    List<Comment> findCommentsByUsername(String username);

    Optional<BlogUser> findByUsername(String username);
}

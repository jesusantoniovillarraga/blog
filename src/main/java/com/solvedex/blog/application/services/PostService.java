package com.solvedex.blog.application.services;

import com.solvedex.blog.domain.entities.BlogUser;
import com.solvedex.blog.domain.entities.Post;
import com.solvedex.blog.infrastructure.repositories.PostRepositoryInterface;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class PostService {

    @Autowired
    private PostRepositoryInterface repository;

    public Post getPostById(Integer id){
        return repository.findById(id).orElseThrow(() -> new EntityNotFoundException("Post not found"));
    }

    public List<Post> getPosts(){
        return repository.findAll();
    }

    public void createPost(Post post){
        post.setCreatedAt(LocalDate.now());
        repository.save(post);
    }

    public void updatePost(Integer id, String title, String content){
        Post postUpdate = getPostById(id);
        postUpdate.setTitle(title);
        postUpdate.setDescription(content);
        postUpdate.setUpdatedAt(LocalDate.now());
        repository.save(postUpdate);
    }

    public void deletePost(Integer id){
        Post postDelete = this.getPostById(id);
        repository.delete(postDelete);
    }
}
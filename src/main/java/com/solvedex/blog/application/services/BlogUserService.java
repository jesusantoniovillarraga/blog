package com.solvedex.blog.application.services;

import com.solvedex.blog.domain.entities.BlogUser;
import com.solvedex.blog.domain.entities.Post;
import com.solvedex.blog.infrastructure.repositories.BlogUserRepositoryInterface;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BlogUserService {

    @Autowired
    private BlogUserRepositoryInterface repository;

    public BlogUser getUserByUsername(String username){
        return repository.findByUsername(username).orElse(null);
    }

    public List<Post> getPostByUsername(String username){
        return repository.findPostsByUsername(username);
    }

    public void createUser(BlogUser user){
        repository.save(user);
    }
}

package com.solvedex.blog.application.services;

import com.solvedex.blog.domain.entities.Comment;
import com.solvedex.blog.infrastructure.repositories.CommentRepositoryInterface;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class CommentService {

    @Autowired
    private CommentRepositoryInterface repository;

    public void createComment(Comment comment){
        comment.setCreatedAt(LocalDate.now());
        repository.save(comment);
    }

    public Comment getCommentById(Integer id){
        return repository.findById(id).orElseThrow(() -> new EntityNotFoundException("Comment not found"));
    }

    public void updateComment(Integer id, String content){
        Comment commentUpdate = this.getCommentById(id);
        commentUpdate.setDescription(content);
        repository.save(commentUpdate);
    }

    public void deleteComment(Integer id){
        Comment commentDelete = this.getCommentById(id);
        repository.delete(commentDelete);
    }
}

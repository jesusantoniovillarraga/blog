package com.solvedex.blog.application.controllers;

import com.solvedex.blog.application.dto.comment.CommentDto;
import com.solvedex.blog.application.dto.post.PostDto;
import com.solvedex.blog.application.usecases.contracts.comment.DeleteCommentUseCaseInterface;
import com.solvedex.blog.application.usecases.contracts.comment.ManageCommentsUseCaseInterface;
import com.solvedex.blog.domain.errors.ApiError;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/comments")
public class CommentController {

    @Autowired
    @Qualifier("createCommentUseCase")
    private ManageCommentsUseCaseInterface createManageUseCase;

    @Autowired
    @Qualifier("deleteCommentUseCase")
    private DeleteCommentUseCaseInterface deleteManageUseCase;

    @Autowired
    @Qualifier("updateCommentUseCase")
    private ManageCommentsUseCaseInterface updateManageUseCase;

    @PostMapping("")
    @Operation(summary = "Create a new comment", description = "Create a new comment", tags = { "Comments" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Successful operation", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = CommentDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Bad Request", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiError.class)) })
    })
    public ResponseEntity<String> createComment(@io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Information object to register new comment",
            required = true) @Valid @RequestBody CommentDto commentDto){
        createManageUseCase.manageComent(commentDto);
        return new ResponseEntity<>("Comment Created Succesfully", HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete a existing comment by id", description = "Delete a existing comment by id", tags = { "Comments" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful operation", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = String.class)) }),
            @ApiResponse(responseCode = "400", description = "Bad Request", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiError.class)) })
    })
    public ResponseEntity<String> deleteCommentById(@Parameter(description = "Id of existing comment", required = true)
                                                  @PathVariable Integer id){
        deleteManageUseCase.deleteComment(id);
        return new ResponseEntity<>("Comment deleted succesfully", HttpStatus.OK);
    }

    @PutMapping("/{id}")
    @Operation(summary = "Update a existing comment by id", description = "Update a existing comment by id", tags = { "Comments" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful operation", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = String.class)) }),
            @ApiResponse(responseCode = "400", description = "Bad Request", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiError.class)) })
    })
    public ResponseEntity<String> updatePosts(@Parameter(description = "Id of existing comment", required = true)
                                              @PathVariable Integer id,@io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Information object to update a comment",
            required = true) @Valid @RequestBody CommentDto commentDto){
        commentDto.setCommentId(String.valueOf(id));
        updateManageUseCase.manageComent(commentDto);
        return new ResponseEntity<>("Comment updated succesfully", HttpStatus.OK);
    }
}

package com.solvedex.blog.application.controllers;

import com.solvedex.blog.application.dto.post.PostDto;
import com.solvedex.blog.application.usecases.contracts.post.DeletePostUseCaseInterface;
import com.solvedex.blog.application.usecases.contracts.post.ManagePostUseCaseInterface;
import com.solvedex.blog.application.usecases.contracts.post.ReadPostUseCaseInterface;
import com.solvedex.blog.domain.errors.ApiError;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/posts")
public class PostController {

    @Autowired
    @Qualifier("readPostUseCase")
    private ReadPostUseCaseInterface useCase;

    @Autowired
    @Qualifier("createPostUseCase")
    private ManagePostUseCaseInterface createManageUseCase;

    @Autowired
    @Qualifier("deletePostUseCase")
    private DeletePostUseCaseInterface deleteManageUseCase;

    @Autowired
    @Qualifier("updatePostUseCase")
    private ManagePostUseCaseInterface updateManageUseCase;

    @GetMapping("")
    @Operation(summary = "List all posts created", description = "List all posts created", tags = { "Posts" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful operation", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = PostDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Bad Request", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiError.class)) })
    })
    public ResponseEntity<List<PostDto>> getAllPosts(){
        List<PostDto> result = useCase.getPosts();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get a existing post by id", description = "Get a existing post by id", tags = { "Posts" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful operation", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = PostDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Bad Request", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiError.class)) })
    })
    public ResponseEntity<PostDto> getPostsById(@Parameter(description = "Id of existing post", required = true)
                                                          @PathVariable Integer id){
        PostDto result = useCase.getPost(id);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping("")
    @Operation(summary = "Create a new post", description = "Create a new post", tags = { "Posts" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Successful operation", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = PostDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Bad Request", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiError.class)) })
    })
    public ResponseEntity<String> createPosts(@io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Information object to register new post",
                                                required = true) @Valid @RequestBody PostDto post){
        createManageUseCase.managePost(post);
        return new ResponseEntity<>("Post Created Succesfully", HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete a existing post by id", description = "Delete a existing post by id", tags = { "Posts" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful operation", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = String.class)) }),
            @ApiResponse(responseCode = "400", description = "Bad Request", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiError.class)) })
    })
    public ResponseEntity<String> deletePostsById(@Parameter(description = "Id of existing post", required = true)
                                                @PathVariable Integer id){
        deleteManageUseCase.deletePost(id);
        return new ResponseEntity<>("Post deleted succesfully", HttpStatus.OK);
    }

    @PutMapping("/{id}")
    @Operation(summary = "Update a existing post by id", description = "Update a existing post by id", tags = { "Posts" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful operation", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = String.class)) }),
            @ApiResponse(responseCode = "400", description = "Bad Request", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiError.class)) })
    })
    public ResponseEntity<String> updatePosts(@Parameter(description = "Id of existing post", required = true)
                                                  @PathVariable Integer id,@io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Information object to update a post",
                                                required = true) @Valid @RequestBody PostDto post){
        post.setPostId(String.valueOf(id));
        updateManageUseCase.managePost(post);
        return new ResponseEntity<>("Post updated succesfully", HttpStatus.OK);
    }
}
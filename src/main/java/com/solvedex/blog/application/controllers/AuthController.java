package com.solvedex.blog.application.controllers;

import com.solvedex.blog.application.dto.authentication.BlogUserAuthenticatedDto;
import com.solvedex.blog.application.dto.authentication.LoginDto;
import com.solvedex.blog.application.dto.authentication.RegisterDto;
import com.solvedex.blog.application.security.JWTGenerator;
import com.solvedex.blog.application.services.BlogUserService;
import com.solvedex.blog.domain.entities.BlogUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    private final AuthenticationManager authenticationManager;
    private final BlogUserService userService;
    private final PasswordEncoder passwordEncoder;
    private JWTGenerator jwtGenerator;

    @Autowired
    public AuthController(AuthenticationManager authenticationManager,
                          BlogUserService userService,
                          PasswordEncoder passwordEncoder,JWTGenerator jwtGenerator) {
        this.authenticationManager = authenticationManager;
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
        this.jwtGenerator = jwtGenerator;
    }

    @PostMapping("register")
    public ResponseEntity<String> register(@RequestBody RegisterDto registerDto){
        if(userService.getUserByUsername(registerDto.getUsername()) != null){
            return new ResponseEntity<>("Username is taken", HttpStatus.BAD_REQUEST);
        }
        BlogUser newUser = new BlogUser();
        newUser.setEmail(registerDto.getEmail());
        newUser.setUsername(registerDto.getUsername());
        newUser.setPassword(passwordEncoder.encode((registerDto.getPassword())));

        userService.createUser(newUser);
        return new ResponseEntity<>("User registration success", HttpStatus.OK);
    }

    @PostMapping("login")
    public ResponseEntity<BlogUserAuthenticatedDto> login(@RequestBody LoginDto loginDto){
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginDto.getUsername(),
                        loginDto.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String token = jwtGenerator.generateToken(authentication);
        return new ResponseEntity<>(new BlogUserAuthenticatedDto(token), HttpStatus.OK);
    }
}

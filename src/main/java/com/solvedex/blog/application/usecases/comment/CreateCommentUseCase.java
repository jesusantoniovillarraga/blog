package com.solvedex.blog.application.usecases.comment;

import com.solvedex.blog.application.dto.comment.CommentDto;
import com.solvedex.blog.application.services.BlogUserService;
import com.solvedex.blog.application.services.CommentService;
import com.solvedex.blog.application.usecases.contracts.comment.ManageCommentsUseCaseInterface;
import com.solvedex.blog.domain.entities.Comment;
import com.solvedex.blog.infrastructure.mappers.CommentMapperInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CreateCommentUseCase implements ManageCommentsUseCaseInterface {

    @Autowired
    private CommentService service;

    @Autowired
    private BlogUserService userService;

    @Override
    public void manageComent(CommentDto commentDto) {
        Comment comment = CommentMapperInterface.MAPPER.toComment(commentDto);
        comment.setBlogUser(userService.getUserByUsername(comment.getBlogUser().getUsername()));
        service.createComment(comment);
    }
}

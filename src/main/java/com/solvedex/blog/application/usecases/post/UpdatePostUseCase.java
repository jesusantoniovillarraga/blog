package com.solvedex.blog.application.usecases.post;

import com.solvedex.blog.application.dto.post.PostDto;
import com.solvedex.blog.application.services.PostService;
import com.solvedex.blog.application.usecases.contracts.post.ManagePostUseCaseInterface;
import com.solvedex.blog.domain.entities.Post;
import com.solvedex.blog.infrastructure.mappers.PostMapperInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UpdatePostUseCase implements ManagePostUseCaseInterface {

    @Autowired
    private PostService service;

    @Override
    public void managePost(PostDto postDto) {
        Post post = PostMapperInterface.MAPPER.toPost(postDto);
        service.updatePost(post.getId(), post.getTitle(), post.getDescription());
    }
}
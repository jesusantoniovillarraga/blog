package com.solvedex.blog.application.usecases.contracts.post;

import com.solvedex.blog.application.dto.post.PostDto;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface ReadPostUseCaseInterface{
    public List<PostDto> getPosts();

    public PostDto getPost(Integer id);
}
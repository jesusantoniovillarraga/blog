package com.solvedex.blog.application.usecases.comment;

import com.solvedex.blog.application.services.CommentService;
import com.solvedex.blog.application.usecases.contracts.comment.DeleteCommentUseCaseInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DeleteCommentUseCase implements DeleteCommentUseCaseInterface {

    @Autowired
    private CommentService service;

    @Override
    public void deleteComment(Integer id) {
        service.deleteComment(id);
    }
}
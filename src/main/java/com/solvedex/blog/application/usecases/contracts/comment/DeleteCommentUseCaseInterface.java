package com.solvedex.blog.application.usecases.contracts.comment;

import org.springframework.stereotype.Component;

@Component
public interface DeleteCommentUseCaseInterface {
    public void deleteComment(Integer id);
}

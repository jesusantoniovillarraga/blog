package com.solvedex.blog.application.usecases.post;

import com.solvedex.blog.application.dto.post.PostDto;
import com.solvedex.blog.application.services.PostService;
import com.solvedex.blog.application.usecases.contracts.post.ReadPostUseCaseInterface;
import com.solvedex.blog.infrastructure.mappers.PostMapperInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ReadPostUseCase implements ReadPostUseCaseInterface {

    @Autowired
    private PostService service;

    @Override
    public List<PostDto> getPosts() {
        return service.getPosts().stream().map(PostMapperInterface.MAPPER::fromPost).toList();
    }

    @Override
    public PostDto getPost(Integer id) {

        return PostMapperInterface.MAPPER.fromPost(service.getPostById(id));
    }
}

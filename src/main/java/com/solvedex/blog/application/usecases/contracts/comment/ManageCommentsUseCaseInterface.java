package com.solvedex.blog.application.usecases.contracts.comment;

import com.solvedex.blog.application.dto.comment.CommentDto;
import org.springframework.stereotype.Component;

@Component
public interface ManageCommentsUseCaseInterface {
    public void manageComent(CommentDto commentDto);
}

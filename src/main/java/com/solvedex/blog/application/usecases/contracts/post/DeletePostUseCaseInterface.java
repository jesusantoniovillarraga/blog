package com.solvedex.blog.application.usecases.contracts.post;

import org.springframework.stereotype.Component;

@Component
public interface DeletePostUseCaseInterface {
    public void deletePost(Integer id);
}

package com.solvedex.blog.application.usecases.post;

import com.solvedex.blog.application.services.PostService;
import com.solvedex.blog.application.usecases.contracts.post.DeletePostUseCaseInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DeletePostUseCase implements DeletePostUseCaseInterface {

    @Autowired
    private PostService service;

    @Override
    public void deletePost(Integer id) {
        service.deletePost(id);
    }
}
package com.solvedex.blog.application.usecases.contracts.post;

import com.solvedex.blog.application.dto.post.PostDto;
import org.springframework.stereotype.Component;

@Component
public interface ManagePostUseCaseInterface {
    public void managePost(PostDto post);
}
package com.solvedex.blog.application.usecases.post;

import com.solvedex.blog.application.dto.post.PostDto;
import com.solvedex.blog.application.services.BlogUserService;
import com.solvedex.blog.application.services.PostService;
import com.solvedex.blog.application.usecases.contracts.post.ManagePostUseCaseInterface;
import com.solvedex.blog.domain.entities.BlogUser;
import com.solvedex.blog.domain.entities.Post;
import com.solvedex.blog.infrastructure.mappers.PostMapperInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CreatePostUseCase implements ManagePostUseCaseInterface {

    @Autowired
    private PostService service;

    @Autowired
    private BlogUserService userService;

    @Override
    public void managePost(PostDto postDto) {
        Post post = PostMapperInterface.MAPPER.toPost(postDto);
        post.setBlogUser(userService.getUserByUsername(post.getBlogUser().getUsername()));
        service.createPost(post);
    }
}

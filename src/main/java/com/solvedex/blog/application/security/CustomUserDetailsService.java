package com.solvedex.blog.application.security;

import com.solvedex.blog.domain.entities.BlogUser;
import com.solvedex.blog.infrastructure.repositories.BlogUserRepositoryInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private BlogUserRepositoryInterface repository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        List<String> roles = new LinkedList<>();
        roles.add("USER");
        BlogUser user = repository.findByUsername(username).orElseThrow(()-> new UsernameNotFoundException("User not found"));
        return new User(user.getUsername(),user.getPassword(), mapRoles(roles));
    }

    private Collection<GrantedAuthority> mapRoles(List<String> roles){
        return roles.stream().map(role->new SimpleGrantedAuthority(role)).collect(Collectors.toList());
    }
}

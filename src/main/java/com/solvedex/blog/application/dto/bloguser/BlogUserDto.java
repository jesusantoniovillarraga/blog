package com.solvedex.blog.application.dto.bloguser;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.solvedex.blog.application.dto.post.PostDto;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class BlogUserDto {

    @JsonProperty("username")
    @Size(min = 0, max = 30)
    @NotNull
    @Schema(
            description = "Existing User username",
            type = "string",
            example = "newuser01"
    )
    private String username;

    @JsonProperty("email")
    @Size(min = 0, max = 255)
    @NotNull
    @Email
    @Schema(
            description = "Existing User email",
            type = "string",
            example = "newuser01@gmail.com"
    )
    private String email;

    @JsonProperty("posts")
    @NotNull
    @Schema(
            description = "Posts made by the user",
            type = "object array"
    )
    List<@Valid PostDto> posts;
}

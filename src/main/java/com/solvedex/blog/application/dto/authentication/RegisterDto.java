package com.solvedex.blog.application.dto.authentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegisterDto {

    @JsonProperty("email")
    @Size(min = 0, max = 255)
    @NotNull
    @Email
    @Schema(
            description = "Existing User email",
            type = "string",
            example = "newuser01@gmail.com"
    )
    private String email;

    @JsonProperty("username")
    @Size(min = 0, max = 30)
    @NotNull
    @Schema(
            description = "Existing User username",
            type = "string",
            example = "newuser01"
    )
    private String username;

    @JsonProperty("password")
    @Size(min = 0, max = 255)
    @NotNull
    @Schema(
            description = "Existing User password",
            type = "string",
            example = "securePassword1!"
    )
    private String password;

    @JsonProperty("password_confirmation")
    @Size(min = 0, max = 255)
    @NotNull
    @Schema(
            description = "User password Confirmation",
            type = "string",
            example = "securePassword1!"
    )
    private String passwordConfirmation;
}

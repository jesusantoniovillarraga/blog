package com.solvedex.blog.application.dto.authentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BlogUserAuthenticatedDto {

    @JsonProperty("tokenType")
    @Size(min = 0, max = 30)
    @NotNull
    @Schema(
            description = "Token type",
            type = "string"
    )
    private String tokenType = "Bearer ";

    @JsonProperty("token")
    @NotNull
    @Schema(
            description = "Valid token for User",
            type = "string"
    )
    private String token;

    public BlogUserAuthenticatedDto(String token) {
        this.token = token;
    }
}

package com.solvedex.blog.application.dto.comment;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommentDto {

    @JsonProperty("comment_id")
    @Schema(
            description = "Existing comment id",
            type = "integer",
            example = "1"
    )
    private String commentId;

    @JsonProperty("username")
    @Size(min = 0, max = 30)
    @NotNull
    @Schema(
            description = "Existing User username",
            type = "string",
            example = "newuser01"
    )
    private String username;

    @JsonProperty("content")
    @Size(min = 0, max = 255)
    @NotNull
    @Schema(
            description = "Comment content",
            type = "string",
            example = "this is a new comment"
    )
    private String content;

    @JsonProperty("post_id")
    @Size(min = 0, max = 255)
    @NotNull
    @Schema(
            description = "Existing post id",
            type = "integer",
            example = "10"
    )
    private String postId;
}

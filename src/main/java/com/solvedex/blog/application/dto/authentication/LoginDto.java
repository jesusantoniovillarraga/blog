package com.solvedex.blog.application.dto.authentication;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginDto {

    @JsonProperty("username")
    @Size(min = 0, max = 30)
    @NotNull
    @Schema(
            description = "Existing User username",
            type = "string",
            example = "newuser01"
    )
    private String username;

    @JsonProperty("password")
    @Size(min = 0, max = 255)
    @NotNull
    @Schema(
            description = "Existing User password",
            type = "string",
            example = "securePassword1!"
    )
    private String password;
}

package com.solvedex.blog.application.dto.comment;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdateComment {

    @JsonProperty("comment_id")
    @NotNull
    @Schema(
            description = "Existing comment id",
            type = "integer",
            example = "1"
    )
    private String commentId;

    @JsonProperty("content")
    @Size(min = 0, max = 255)
    @NotNull
    @Schema(
            description = "Comment content",
            type = "string",
            example = "this is a new comment"
    )
    private String content;
}

package com.solvedex.blog.application.dto.post;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.solvedex.blog.application.dto.comment.CommentDto;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
public class PostDto {

    @JsonProperty("post_id")
    @Schema(
            description = "Existing comment id",
            type = "integer",
            example = "1"
    )
    private String postId;

    @JsonProperty("username")
    @Size(min = 0, max = 30)
    @NotNull
    @Schema(
            description = "Existing User username",
            type = "string",
            example = "newuser01"
    )
    private String username;

    @JsonProperty("title")
    @Size(min = 0, max = 255)
    @NotNull
    @Schema(
            description = "Post title",
            type = "string",
            example = "this is the title"
    )
    private String title;

    @JsonProperty("content")
    @Size(min = 0, max = 255)
    @NotNull
    @Schema(
            description = "Post content",
            type = "string",
            example = "this is a new post"
    )
    private String content;

    @JsonProperty("creationDate")
    @Schema(
            description = "Post's creation date",
            type = "string",
            format = "date-time",
            example = "2020-04-28"
    )
    private LocalDate creationDate;

    @JsonProperty("comments")
    @Schema(
            description = "Comments in the post",
            type = "object array"
    )
    List<@Valid CommentDto> comments;
}

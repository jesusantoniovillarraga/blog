package com.solvedex.blog.domain.entities;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Entity
@Getter
@Setter
public class BlogUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @Pattern(regexp = "([A-Za-z0-9-_]+)")
    @Column(unique=true , length = 30)
    private String username;

    @NotNull
    @Column(length = 255)
    private String password;

    @NotNull
    @Column(unique=true, length = 255)
    private String email;

    @OneToMany(mappedBy="blogUser")
    private List<Post> posts;

    @OneToMany(mappedBy="blogUser")
    private List<Comment> comments;
}

package com.solvedex.blog.domain.entities;

import jakarta.persistence.*;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Entity
@Getter
@Setter
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @Pattern(regexp = "([A-Za-z0-9\\s-_]+)")
    @Column(length = 30)
    private String title;

    @NotNull
    @Column(columnDefinition = "TEXT")
    private String description;

    @NotNull
    private LocalDate createdAt;

    @Column(nullable = true)
    private LocalDate updatedAt;

    @ManyToOne
    @JoinColumn(name="blog_user.id", nullable=false)
    private BlogUser blogUser;

    @OneToMany(mappedBy="post")
    private List<Comment> comments;
}

package com.solvedex.blog.domain.entities;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @Column(columnDefinition = "TEXT")
    private String description;

    @NotNull
    private LocalDate createdAt;

    @Column(nullable = true)
    private LocalDate updatedAt;

    @ManyToOne
    @JoinColumn(name="blog_user.id", nullable=false)
    private BlogUser blogUser;

    @ManyToOne
    @JoinColumn(name="post.id", nullable=false)
    private Post post;
}

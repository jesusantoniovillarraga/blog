package com.solvedex.blog.domain.errors;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
public class ApiError {

    @Schema(
            description = "Http error",
            type = "string"
    )
    private HttpStatus status;

    @Schema(
            description = "Error creation date",
            type = "string",
            format = "date-time",
            example = "2020-04-28T00:00:00.000000"
    )
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private LocalDateTime timestamp;

    @Schema(
            description = "Error message",
            type = "string",
            example = "Object can't be loaded"
    )
    private String message;

    @Schema(
            description = "Error debug message",
            type = "string",
            example = "Object not found"
    )
    private String debugMessage;

    @Schema(
            description = "Sub errors that can be appear",
            type = "list",
            example = "[{fielname: can not be null}]"
    )
    private List<ApiSubError> subErrors;

    private ApiError() {
        timestamp = LocalDateTime.now();
    }

    public ApiError(HttpStatus status) {
        this();
        this.status = status;
    }

    public ApiError(HttpStatus status, Throwable ex) {
        this();
        this.status = status;
        this.message = "Unexpected error";
        this.debugMessage = ex.getLocalizedMessage();
    }

    public ApiError(HttpStatus status, String message, Throwable ex) {
        this();
        this.status = status;
        this.message = message;
        this.debugMessage = ex.getLocalizedMessage();
    }
}

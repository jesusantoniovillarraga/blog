# Solvedex Technical Test

## Blog
This project is a technical test for solvedex soft to show my ability with Java, Spring boot and hibernate

## Description
The objetive is create a API Rest to manage a blog posts and comments

## Requirements
For building and running the application you need:

- [Open JDK](https://jdk.java.net/archive/)
- [Gradle](https://gradle.org/releases/)

## Installation
Use git to clone this repository into your computer.

Using HTTPS
```
git clone https://gitlab.com/jesusantoniovillarraga/blog.git
```
Using SSH
```
git clone git@gitlab.com:jesusantoniovillarraga/blog.git
```

## Usage
This application is packaged as a jar which has Tomcat 8 embedded. No Tomcat or JBoss installation is necessary. You run it using the java -jar command.

You can build the project and run the tests by running
```
gradle build
```
Once successfully built, you can run the service running
```
gradle bootRun
```
Once the application runs you should see something like this
```
2023-04-17T16:35:35.552-05:00  INFO 17488 --- [  restartedMain] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path ''
2023-04-17T16:35:35.558-05:00  INFO 17488 --- [  restartedMain] com.solvedex.blog.BlogApplication        : Started BlogApplication in 1.543 seconds (process running for 1.809)
```
***
## About the project
It uses an in-memory database (H2) to store the data. If your database connection properties work, you can call some endpoints defined in com.solvedex.blog.controller on port 8080.

You can get more information about the endpoints in [Swagger-UI](http://localhost:8080/swagger-ui/index.html) when you run this project.

You can connect to H2 Database in [H2 Console](http://localhost:8080/h2-console/) when you run this project.

You can get more information about the proyect and diagrams inside "src/main/resources/diagrams/".

Finally you can get a postman collection with all endpoint and its parameter inside "src/main/resources/Blog_collection.json"